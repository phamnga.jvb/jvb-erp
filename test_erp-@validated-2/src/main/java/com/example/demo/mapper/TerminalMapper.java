package com.example.demo.mapper;

import java.math.BigInteger;
import java.util.List;

import com.example.demo.model.Terminal;

public interface TerminalMapper {

	public List<Terminal> getAll();
	
	public List<Terminal> searchByTerminal(Terminal terminal);
	
	public void deleteById (BigInteger id);
	
	public void add (Terminal terminal);
	
	public List<Terminal> getTerminalByMtid(String mtid);
	
	public void updateById (Terminal terminal);
}
