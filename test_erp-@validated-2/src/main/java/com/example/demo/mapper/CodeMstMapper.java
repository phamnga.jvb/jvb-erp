package com.example.demo.mapper;

import java.util.List;

import com.example.demo.model.CodeMst;

public interface CodeMstMapper {

	public List<CodeMst> getAll ();
	
	public List<CodeMst> findCodeMstByCodeType (String codeType);
	
	public void deleteCodeMst (String codeName);
	
	public void editCodeMst (CodeMst codeMst);
	
	public void addCodeMst (CodeMst codeMst);
	
	public List<CodeMst> findCodeMstByCodeName (String codeName);
	
	public List<CodeMst> findCodeMstByCodeValue (String codeValue);
	
	public List<CodeMst> findCodeMstBySortIndex (int sortIndex);
	
	public List<CodeMst> findCodeMstByRemarks (String remarks);

	public List<CodeMst> findCodeMst (CodeMst codeMst);
	
	public void editCodeMstBySortIndex (CodeMst codeMst);

}
