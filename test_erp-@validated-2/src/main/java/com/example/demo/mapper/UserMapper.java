package com.example.demo.mapper;

import java.util.List;

import com.example.demo.model.User;

public interface UserMapper {

	public List<User> getUserByEmail(String email);
	
	public void saveUser (User user);
	
	public List<User> getUserByResetToken(String resetToken);
	
	public void changeResetToken (User user);
	
	public void changePassword (User user);
}
