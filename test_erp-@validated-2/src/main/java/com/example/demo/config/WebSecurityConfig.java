package com.example.demo.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import com.example.demo.service.UserDetailsServiceImpl;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired
	UserDetailsServiceImpl userDetailsService;
	
	@Bean
	@Override
	public AuthenticationManager authenticationManagerBean () throws Exception {
		
		return super.authenticationManagerBean();
	}
	
	@Bean
	public BCryptPasswordEncoder passwordEncoder () {
		BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
		
		return bCryptPasswordEncoder;
	}
	
	@Autowired
	public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder());
	}
	
	@Override
	protected void configure (HttpSecurity http) throws Exception {
		http.csrf().disable();
		
		// ALL USER
		http.
			authorizeRequests()
				.antMatchers("/", "/login", "/logout", "/register")
				.permitAll();
		
		// ROLE USER + ROLE ADMIN
		http
			.authorizeRequests()
			.antMatchers("/welcome",
					"/codeMst/view", "/codeMst/findCodeMst", 
					"/terminal/view", "/terminal/search")
			.access("hasAnyRole('ROLE_USER','ROLE_ADMIN')");
	
		// ROLE ADMIN
		http
			.authorizeRequests()
			.antMatchers("/admin", 
					"/codeMst/add", "/codeMst/edit", "/codeMst/delete",
					"/terminal/add", "/terminal/view-add", "/terminal/view-add-verify", "/terminal/delete", "/terminal/verify", 
					"/terminal/view-update", "/terminal/view-update-2", "/terminal/view-update-verify", "/terminal/update")
			.access("hasRole('ROLE_ADMIN')");
		
		// LOGIN PAGE, LOGOUT PAGE, WELCOME PAGE
		http
			.authorizeRequests()
			.and()
				.formLogin()
				.loginProcessingUrl("/login")
				.loginPage("/login")
				.defaultSuccessUrl("/welcome")
				.failureUrl("/login")
				.usernameParameter("email")
				.passwordParameter("password")
			.and()
				.logout()
				.logoutUrl("/logout")
				.logoutSuccessUrl("/welcome");
		
//		http
//			.authorizeRequests()
//				.antMatchers("/register").permitAll()
//				.anyRequest().authenticated()
//			.and()
//				.formLogin()
//				.loginProcessingUrl("/login")
//				.loginPage("/login")
//				.defaultSuccessUrl("/welcome")
//				.failureUrl("/login")
//				.usernameParameter("email")
//				.passwordParameter("password")
//			.and()
//				.logout()
//				.logoutUrl("/logout")
//				.logoutSuccessUrl("/welcome");
		
		// ACCESS DENIDED
		http
			.authorizeRequests().and().exceptionHandling().accessDeniedPage("/403");
	}
}
