package com.example.demo.service;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.example.demo.mapper.CodeMstMapper;
import com.example.demo.model.CodeMst;

@Service
public class CodeMstService {

	@Resource
	private CodeMstMapper codeMstMapper;
	
	public List<CodeMst> getAll () {
		
		return codeMstMapper.getAll();
	}
	
	public void editCodeMst (CodeMst codeMst) {
		
		codeMstMapper.editCodeMst(codeMst);
	}
	
	public List<CodeMst> findCodeMst (CodeMst codeMst) {
		
		return codeMstMapper.findCodeMst(codeMst);
	}
	
	public List<CodeMst> findCodeMstByCodeType (String codeType) {
		
		return codeMstMapper.findCodeMstByCodeType(codeType);
	}
	
	public List<CodeMst> findCodeMstByCodeValue(String codeValue) {

		return codeMstMapper.findCodeMstByCodeValue(codeValue);
	}
	
	public void deleteCodeMst (String codeName) {
		
		codeMstMapper.deleteCodeMst(codeName);
	}
	
	public void addCodeMst (CodeMst codeMst) {
		
		codeMstMapper.addCodeMst(codeMst);
	}
	
	public List<CodeMst> findCodeMstByCodeName (String codeName) {
		
		return codeMstMapper.findCodeMstByCodeName(codeName);
	}
	
	public List<CodeMst> findCodeMstBySortIndex(int sortIndex) {

		return codeMstMapper.findCodeMstBySortIndex(sortIndex);
	}

	public List<CodeMst> findCodeMstByRemarks(String remarks) {

		return codeMstMapper.findCodeMstByRemarks(remarks);
	}
	
	public void editCodeMstBySortIndex (CodeMst codeMst) {
		
		codeMstMapper.editCodeMstBySortIndex(codeMst);
	}
	
}
