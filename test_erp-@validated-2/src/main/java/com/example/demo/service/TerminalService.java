package com.example.demo.service;

import java.math.BigInteger;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.mapper.TerminalMapper;
import com.example.demo.model.Terminal;

@Service
public class TerminalService {

	@Autowired
	private TerminalMapper terminalMapper;
	
	public List<Terminal> getAll() {
		
		return terminalMapper.getAll();
	}
	
	public List<Terminal> searchByTerminal(Terminal terminal) {
		
		return terminalMapper.searchByTerminal(terminal);
	}
	
	public void deleteById (BigInteger id) {
		
		terminalMapper.deleteById(id);
	}
	
	public void add (Terminal terminal) {
		
		terminalMapper.add(terminal);
	}
	
	public List<Terminal> getTerminalByMtid(String mtid) {
		
		return terminalMapper.getTerminalByMtid(mtid);
	}
	
	public void updateById (Terminal terminal) {
		
		terminalMapper.updateById(terminal);
	}
}
