package com.example.demo.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.example.demo.mapper.UserMapper;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

	@Autowired
	private UserMapper userMapper;
	
	@Override
	public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
		List<com.example.demo.model.User> listUser = userMapper.getUserByEmail(email);
		
		if (listUser.size() == 0) {
			System.out.println("User not found! " + email);
			throw new UsernameNotFoundException("User " + email + " was not found in the database!");
		} else {
		
			// Notify
			System.out.println("Found User: " + listUser.get(0).getUserName());
		}
		
		List<GrantedAuthority> grandList = new ArrayList<>();
		
		if (listUser.get(0).getRoleName() != null) {
			GrantedAuthority authority = new SimpleGrantedAuthority(listUser.get(0).getRoleName());
			grandList.add(authority);
		}
		
//		System.out.println(grandList);

		UserDetails userDetails = new User(listUser.get(0).getEmail(),
				listUser.get(0).getPassword(), grandList);
		
		return userDetails;
	}

}
