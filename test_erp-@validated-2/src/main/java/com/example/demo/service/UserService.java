package com.example.demo.service;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.example.demo.mapper.UserMapper;
import com.example.demo.model.User;

@Service
public class UserService {

	@Resource
	private UserMapper userMapper;
	
	@Autowired
	private BCryptPasswordEncoder bCryptPasswordEncoder;
	
	public List<User> getUserByEmail(String email) {
		
		return userMapper.getUserByEmail(email);
	}
	
	public void saveUser (User user) {
		user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
		user.setPasswordConfirm(bCryptPasswordEncoder.encode(user.getPasswordConfirm()));
		
		userMapper.saveUser(user);
	}
	
	public List<User> getUserByResetToken(String resetToken) {
		
		return userMapper.getUserByResetToken(resetToken);
	}
	
	public void changeResetToken (User user) {
		userMapper.changeResetToken(user);
	}
	
	public void changePassword (User user) {
		user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
		user.setPasswordConfirm(bCryptPasswordEncoder.encode(user.getPasswordConfirm()));
		
		userMapper.changePassword(user);
	}
}
