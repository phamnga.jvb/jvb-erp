package com.example.demo.model;

import java.sql.Timestamp;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.Length;
import org.springframework.stereotype.Component;

@Component
public class CodeMst {

	private int iD;
	private Timestamp updateTimestamp;
	private Timestamp insertTimestamp;
	private String updateUser;
	private String insertUser;
	private String status;
	private int version;
	
	@NotBlank(message = "{NotEmpty.codeMst.codeType}")
	@Length(max = 32, message = "{Size.Max.codeMst.codeType}")
	private String codeType;
	
	@NotBlank(message = "{NotEmpty.codeMst.codeName}")
	@Length(max = 127, message = "{Size.Max.codeMst.codeName}")
	private String codeName;
	
	@NotBlank(message = "{NotEmpty.codeMst.codeValue}")
	@Length(max = 127, message = "{Size.Max.codeMst.codeValue}")
	private String codeValue;
	
	@Min(value = 0, message = "{Min.codeMst.sortIndex}")
	private int sortIndex;
	
	@NotBlank(message = "{NotBlank.codeMst.remarks}")
	@Pattern(regexp = "^[a-zA-Z0-9\\s]*$", message = "{Pattern.codeMst.remarks}")
	@Length(max = 127, message = "{Size.Max.codeMst.remarks}")
	private String remarks;
	
	/**
	 * @param iD
	 * @param updateTimestamp
	 * @param insertTimestamp
	 * @param updateUser
	 * @param insertUser
	 * @param status
	 * @param version
	 * @param codeType
	 * @param codeName
	 * @param codeValue
	 * @param sortIndex
	 * @param remarks
	 */
	public CodeMst(int iD, Timestamp updateTimestamp, Timestamp insertTimestamp, String updateUser, String insertUser,
			String status, int version, String codeType, String codeName, String codeValue, int sortIndex,
			String remarks) {
		super();
		this.iD = iD;
		this.updateTimestamp = updateTimestamp;
		this.insertTimestamp = insertTimestamp;
		this.updateUser = updateUser;
		this.insertUser = insertUser;
		this.status = status;
		this.version = version;
		this.codeType = codeType;
		this.codeName = codeName;
		this.codeValue = codeValue;
		this.sortIndex = sortIndex;
		this.remarks = remarks;
	}	
	
	/**
	 * @param codeType
	 * @param codeName
	 * @param codeValue
	 * @param sortIndex
	 * @param remarks
	 */
	public CodeMst(String codeType, String codeName, String codeValue, int sortIndex, String remarks) {
		super();
		this.codeType = codeType;
		this.codeName = codeName;
		this.codeValue = codeValue;
		this.sortIndex = sortIndex;
		this.remarks = remarks;
	}

	/**
	 * 
	 */
	public CodeMst() {
		super();
		// TODO Auto-generated constructor stub
	}

	public int getiD() {
		return iD;
	}

	public void setiD(int iD) {
		this.iD = iD;
	}

	public Timestamp getUpdateTimestamp() {
		return updateTimestamp;
	}

	public void setUpdateTimestamp(Timestamp updateTimestamp) {
		this.updateTimestamp = updateTimestamp;
	}

	public Timestamp getInsertTimestamp() {
		return insertTimestamp;
	}

	public void setInsertTimestamp(Timestamp insertTimestamp) {
		this.insertTimestamp = insertTimestamp;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public String getInsertUser() {
		return insertUser;
	}

	public void setInsertUser(String insertUser) {
		this.insertUser = insertUser;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public String getCodeType() {
		return codeType;
	}

	public void setCodeType(String codeType) {
		this.codeType = codeType;
	}

	public String getCodeName() {
		return codeName;
	}

	public void setCodeName(String codeName) {
		this.codeName = codeName;
	}

	public String getCodeValue() {
		return codeValue;
	}

	public void setCodeValue(String codeValue) {
		this.codeValue = codeValue;
	}

	public int getSortIndex() {
		return sortIndex;
	}

	public void setSortIndex(int sortIndex) {
		this.sortIndex = sortIndex;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	@Override
	public String toString() {
		return "CodeMst [iD=" + iD + ", updateTimestamp=" + updateTimestamp + ", insertTimestamp=" + insertTimestamp
				+ ", updateUser=" + updateUser + ", insertUser=" + insertUser + ", status=" + status + ", version="
				+ version + ", codeType=" + codeType + ", codeName=" + codeName + ", codeValue=" + codeValue
				+ ", sortIndex=" + sortIndex + ", remarks=" + remarks + "]";
	}
		
}
