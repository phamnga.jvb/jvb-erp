package com.example.demo.model;

import java.util.List;

public class AjaxJsonResponse {

	String msg;
	List<CodeMst> result;
	
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public List<CodeMst> getResult() {
		return result;
	}
	public void setResult(List<CodeMst> result) {
		this.result = result;
	}
	
	@Override
	public String toString() {
		return "AjaxJsonResponse [msg=" + msg + ", result=" + result + "]";
	}
	
}
