package com.example.demo.model;

import java.math.BigInteger;
import java.sql.Date;
import java.sql.Timestamp;

import javax.validation.constraints.NotBlank;

import org.hibernate.validator.constraints.Length;

public class Terminal {

	private BigInteger id;
	private Timestamp updateTimestamp;
	private Timestamp insertTimestamp;
	private String updateUser;
	private String insertUser;
	private char status;
	private int version;
	
	@NotBlank(message = "{NotBlank.terminal.clientCode}")
	@Length(max = 7, message = "{Length.terminal.clientCode}")
	private String clientCode;
	
	@NotBlank(message = "{NotBlank.terminal.shopId}")
	@Length(max = 7, message = "{Length.terminal.shopId}")
	private String shopId;
	
	@Length(max = 4, message = "{Length.terminal.sId}")
	private String sId;
	
	@NotBlank(message = "{NotBlank.terminal.mtid}")
	@Length(max = 13, message = "{Length.terminal.mtid}")
	private String mtid;
	
	@Length(max = 13, message = "{Length.terminal.creditTid}")
	private String creditTid;
	
	@NotBlank(message = "{NotBlank.terminal.terminalModel}")
	private String terminalModel;
	
	@Length(max = 32, message = "{Length.terminal.posModel}")
	private String posModel;
	
	private String posConnType;
	private char msrSupport;
	private char confirmAmountFlag;
	private String services;
	private Date reqDeliveryDate;
	private Date cancelDate;
	private Date reqInstallDate;
	private Date reqStartDate;
	private Date startDate;
	
	@Length(max = 256, message = "{Length.terminal.message}")
	private String message;
	
	/**
	 * @param id
	 * @param updateTimestamp
	 * @param insertTimestamp
	 * @param updateUser
	 * @param insertUser
	 * @param status
	 * @param version
	 * @param clientCode
	 * @param shopId
	 * @param mtid
	 * @param creditTid
	 * @param terminalModel
	 * @param posModel
	 * @param posConnType
	 * @param msrSupport
	 * @param confirmAmountFlag
	 * @param services
	 * @param reqDeliveryDate
	 * @param cancelDate
	 * @param reqInstallDate
	 * @param reqStartDate
	 * @param startDate
	 * @param message
	 */
	public Terminal(BigInteger id, Timestamp updateTimestamp, Timestamp insertTimestamp, String updateUser,
			String insertUser, char status, int version, String clientCode, String shopId, String sId, String mtid,
			String creditTid, String terminalModel, String posModel, String posConnType, char msrSupport,
			char confirmAmountFlag, String services, Date reqDeliveryDate, Date cancelDate, Date reqInstallDate,
			Date reqStartDate, Date startDate, String message) {
		super();
		this.id = id;
		this.updateTimestamp = updateTimestamp;
		this.insertTimestamp = insertTimestamp;
		this.updateUser = updateUser;
		this.insertUser = insertUser;
		this.status = status;
		this.version = version;
		this.clientCode = clientCode;
		this.shopId = shopId;
		this.sId = sId;
		this.mtid = mtid;
		this.creditTid = creditTid;
		this.terminalModel = terminalModel;
		this.posModel = posModel;
		this.posConnType = posConnType;
		this.msrSupport = msrSupport;
		this.confirmAmountFlag = confirmAmountFlag;
		this.services = services;
		this.reqDeliveryDate = reqDeliveryDate;
		this.cancelDate = cancelDate;
		this.reqInstallDate = reqInstallDate;
		this.reqStartDate = reqStartDate;
		this.startDate = startDate;
		this.message = message;
	}	
	
	/**
	 * @param clientCode
	 * @param shopId
	 * @param mtid
	 * @param creditTid
	 * @param terminalModel
	 * @param posModel
	 * @param services
	 */
	public Terminal(String clientCode, String shopId, char status, String mtid, String creditTid, String terminalModel,
			String posModel, String services) {
		super();
		this.clientCode = clientCode;
		this.shopId = shopId;
		this.status = status;
		this.mtid = mtid;
		this.creditTid = creditTid;
		this.terminalModel = terminalModel;
		this.posModel = posModel;
		this.services = services;
	}
	
	/**
	 * 
	 */
	public Terminal() {
		super();
		// TODO Auto-generated constructor stub
	}

	public BigInteger getId() {
		return id;
	}

	public void setId(BigInteger id) {
		this.id = id;
	}

	public Timestamp getUpdateTimestamp() {
		return updateTimestamp;
	}

	public void setUpdateTimestamp(Timestamp updateTimestamp) {
		this.updateTimestamp = updateTimestamp;
	}

	public Timestamp getInsertTimestamp() {
		return insertTimestamp;
	}

	public void setInsertTimestamp(Timestamp insertTimestamp) {
		this.insertTimestamp = insertTimestamp;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public String getInsertUser() {
		return insertUser;
	}

	public void setInsertUser(String insertUser) {
		this.insertUser = insertUser;
	}

	public char getStatus() {
		return status;
	}

	public void setStatus(char status) {
		this.status = status;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public String getClientCode() {
		return clientCode;
	}

	public void setClientCode(String clientCode) {
		this.clientCode = clientCode;
	}

	public String getShopId() {
		return shopId;
	}

	public void setShopId(String shopId) {
		this.shopId = shopId;
	}

	public String getMtid() {
		return mtid;
	}

	public void setMtid(String mtid) {
		this.mtid = mtid;
	}

	public String getCreditTid() {
		return creditTid;
	}

	public void setCreditTid(String creditTid) {
		this.creditTid = creditTid;
	}

	public String getTerminalModel() {
		return terminalModel;
	}

	public void setTerminalModel(String terminalModel) {
		this.terminalModel = terminalModel;
	}

	public String getPosModel() {
		return posModel;
	}

	public void setPosModel(String posModel) {
		this.posModel = posModel;
	}

	public String getPosConnType() {
		return posConnType;
	}

	public void setPosConnType(String posConnType) {
		this.posConnType = posConnType;
	}

	public char getMsrSupport() {
		return msrSupport;
	}

	public void setMsrSupport(char msrSupport) {
		this.msrSupport = msrSupport;
	}

	public char getConfirmAmountFlag() {
		return confirmAmountFlag;
	}

	public void setConfirmAmountFlag(char confirmAmountFlag) {
		this.confirmAmountFlag = confirmAmountFlag;
	}

	public String getServices() {
		return services;
	}

	public void setServices(String services) {
		this.services = services;
	}

	public Date getReqDeliveryDate() {
		return reqDeliveryDate;
	}

	public void setReqDeliveryDate(Date reqDeliveryDate) {
		this.reqDeliveryDate = reqDeliveryDate;
	}

	public Date getCancelDate() {
		return cancelDate;
	}

	public void setCancelDate(Date cancelDate) {
		this.cancelDate = cancelDate;
	}

	public Date getReqInstallDate() {
		return reqInstallDate;
	}

	public void setReqInstallDate(Date reqInstallDate) {
		this.reqInstallDate = reqInstallDate;
	}

	public Date getReqStartDate() {
		return reqStartDate;
	}

	public void setReqStartDate(Date reqStartDate) {
		this.reqStartDate = reqStartDate;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getsId() {
		return sId;
	}

	public void setsId(String sId) {
		this.sId = sId;
	}

	@Override
	public String toString() {
		return "Terminal [id=" + id + ", updateTimestamp=" + updateTimestamp + ", insertTimestamp=" + insertTimestamp
				+ ", updateUser=" + updateUser + ", insertUser=" + insertUser + ", status=" + status + ", version="
				+ version + ", clientCode=" + clientCode + ", shopId=" + shopId + ", sId=" + sId + ", mtid=" + mtid
				+ ", creditTid=" + creditTid + ", terminalModel=" + terminalModel + ", posModel=" + posModel
				+ ", posConnType=" + posConnType + ", msrSupport=" + msrSupport + ", confirmAmountFlag="
				+ confirmAmountFlag + ", services=" + services + ", reqDeliveryDate=" + reqDeliveryDate
				+ ", cancelDate=" + cancelDate + ", reqInstallDate=" + reqInstallDate + ", reqStartDate=" + reqStartDate
				+ ", startDate=" + startDate + ", message=" + message + "]";
	}

}
