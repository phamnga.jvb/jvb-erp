package com.example.demo.model;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

import org.springframework.stereotype.Component;

@Component
public class User {

	private int id;
	
	@NotBlank(message = "{NotBlank.user.email}")
	@Email(message = "{Email.regex.error}")
	private String email;
	
	@NotBlank(message = "{NotBlank.user.userName}")
	private String userName;
	
	@NotBlank(message = "{NotBlank.user.password}")
	private String password;
	
	@NotBlank(message = "{NotBlank.user.passwordConfirm}")
	private String passwordConfirm;
	
	private String roleName;
	
	private String resetToken;

	/**
	 * @param id
	 * @param email
	 * @param userName
	 * @param password
	 * @param passwordConfirm
	 * @param roleName
	 * @param resetToken
	 */
	public User(int id, String email, String userName, String password, String passwordConfirm, String roleName,
			String resetToken) {
		super();
		this.id = id;
		this.email = email;
		this.userName = userName;
		this.password = password;
		this.passwordConfirm = passwordConfirm;
		this.roleName = roleName;
		this.resetToken = resetToken;
	}

	/**
	 * 
	 */
	public User() {
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPasswordConfirm() {
		return passwordConfirm;
	}

	public void setPasswordConfirm(String passwordConfirm) {
		this.passwordConfirm = passwordConfirm;
	}

	public String getResetToken() {
		return resetToken;
	}

	public void setResetToken(String resetToken) {
		this.resetToken = resetToken;
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	@Override
	public String toString() {
		return "User [id=" + id + ", email=" + email + ", userName=" + userName + ", password=" + password
				+ ", passwordConfirm=" + passwordConfirm + ", roleName=" + roleName + ", resetToken=" + resetToken
				+ "]";
	}
	
}
