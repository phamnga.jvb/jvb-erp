package com.example.demo.resource;

import java.security.Principal;
import java.util.List;
import java.util.UUID;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.web.authentication.WebAuthenticationDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.example.demo.service.EmailService;
import com.example.demo.service.UserService;

@Controller
public class UserResource {

	@Autowired
	private UserService userService;
	
	@Autowired
	private EmailService emailService;
	
	@Autowired
	protected AuthenticationManager authenticationManager;
	
	@RequestMapping(value = {"/login"}, method = RequestMethod.GET)
	public String login (HttpSession session, Principal principal) {
		
		if (principal != null) {
			User loginedUser = (User) ((Authentication) principal).getPrincipal();
			
			String email = loginedUser.getUsername();
			List<com.example.demo.model.User> listUser = userService.getUserByEmail(email);
			String userInfo = listUser.get(0).getUserName();
			session.setAttribute("userInfo", userInfo);
			session.setAttribute("user", listUser.get(0));
		}
		
		return "login";
	}
	
	@RequestMapping(value = {"/403"}, method = RequestMethod.POST)
	public String accessDenied (Model model, Principal principal) {
		
		if (principal != null) {
			User loginedUser = (User) ((Authentication) principal).getPrincipal();
			
			String email = loginedUser.getUsername();
			List<com.example.demo.model.User> listUser = userService.getUserByEmail(email);
			String userInfo = listUser.get(0).getUserName();
			model.addAttribute("userInfo", userInfo);
			
			String message = "Sorry " + userInfo + ", You do not have permission to access this page!";
			model.addAttribute("message", message);
		}
		
		
		return "403Page";
	}
	
	@RequestMapping(value = {"/register"}, method = RequestMethod.GET)
	public String requestMappingPage (Model model) {
		
		model.addAttribute("user", new com.example.demo.model.User());
		
		return "register";
	}
	
	@RequestMapping(value = {"/register"}, method = RequestMethod.POST)
	public String requestMappingCheck (@ModelAttribute("user") @Validated com.example.demo.model.User userForm,
			BindingResult bindingResult, HttpSession session, HttpServletRequest req) {
		
		if (!userForm.getPassword().equals(userForm.getPasswordConfirm())) {
			bindingResult.addError(new ObjectError("passwordConfirm", "Diff.user.passwordConfirm"));
			bindingResult.rejectValue("passwordConfirm", "Diff.user.passwordConfirm");
		}
		
		List<com.example.demo.model.User> listUser = userService.getUserByEmail(userForm.getEmail());
		
		if (listUser.size() > 0) {
			bindingResult.addError(new ObjectError("email", "Duplicate.user.email"));
			bindingResult.rejectValue("email", "Duplicate.user.email");
		}
		
		if (bindingResult.hasErrors()) {
					
			return "register";
		}
		
		userForm.setRoleName("ROLE_USER");
		userService.saveUser(userForm);
		
		String userInfo = userForm.getUserName();
		session.setAttribute("userInfo", userInfo);
				
		return "redirect:/welcome";
	}
	
	@RequestMapping(value = {"/forgot-password"}, method = RequestMethod.GET)
	public String forgotPage () {
		
		return "forgot-password";
	}
	
	@RequestMapping(value = {"/forgot-password"}, method = RequestMethod.POST)
	public String forgotPassword (HttpServletRequest req, Model model) {
		
		String email = req.getParameter("email");
		List<com.example.demo.model.User> listUser = userService.getUserByEmail(email);
		
		if (listUser.size() <= 0) {
			model.addAttribute("errorMessage", "We didn't find an account for that email address!");
		} else {
			
			com.example.demo.model.User user = listUser.get(0);
			user.setResetToken(UUID.randomUUID().toString());
			
			// CHANGE USER TO DATABASE
			userService.changeResetToken(user);
			
			String appUrl = req.getScheme() + "://" + req.getServerName() + ":" + req.getServerPort();
			
			// EMAIL MESSAGE
			SimpleMailMessage passwordResetEmail = new SimpleMailMessage();
			passwordResetEmail.setTo(user.getEmail());
			passwordResetEmail.setSubject("Password Reset Request");
			passwordResetEmail.setText("To reset your password, click the link below: \n"
					+ appUrl +"/reset?token=" + user.getResetToken());
			
			emailService.sendEmail(passwordResetEmail);
			
			model.addAttribute("successMessage", "A password reset link has been sent to " + email);
		}
		
		return "forgot-password";
	}
	
	@RequestMapping(value = {"/reset"}, method = RequestMethod.GET)
	public String changePasswordPage (HttpServletRequest req, Model model) {
		
		String resetToken = req.getParameter("token");
		
		// FIND USER BY RESET TOKEN
		List<com.example.demo.model.User> listUser = userService.getUserByResetToken(resetToken);
		
		if (listUser.size() <= 0) {
			model.addAttribute("errorMessage", "Oops!  This is an invalid password reset link.");
		} else {
			model.addAttribute("resetToken", resetToken);
			model.addAttribute("user", listUser.get(0));
		}
		
		return "reset-password";
	}
	
	@RequestMapping(value = {"/reset"}, method = RequestMethod.POST)
	public String changePassword (HttpServletRequest req, Model model,
			@Validated @ModelAttribute ("user") com.example.demo.model.User user,
			BindingResult bindingResult, HttpSession session) {
		
		String resetToken = req.getParameter("token");
		
		// FIND USER BY RESET TOKEN
		List<com.example.demo.model.User> listUser = userService.getUserByResetToken(resetToken);
		
		if (listUser.size() <= 0) {
			model.addAttribute("errorMessage", "Oops!  This is an invalid password reset link.");
			
			return "reset-password";
		} 
		
		// CHECK FIELD PASSWORD AND PASSWORD CONFIRM IS MATCH???
		if (!user.getPassword().equals(user.getPasswordConfirm())) {
			bindingResult.addError(new ObjectError("passwordConfirm", "Diff.user.passwordConfirm"));
			bindingResult.rejectValue("passwordConfirm", "Diff.user.passwordConfirm");
		}
		
		if (bindingResult.hasErrors()) {
			
			return "reset-password";
		}
		
		// IF PASSWORD AND PASSWORD CONFIRM IS MATCH
		user.setResetToken(null);
		userService.changePassword(user);
		
		model.addAttribute("resetToken", resetToken);
		
		String userInfo = user.getUserName();
		session.setAttribute("userInfo", userInfo);
		session.setAttribute("user", user);
		
		return "redirect:/welcome";
	}
	
	public void authWithAuthManager (HttpServletRequest req, String email, String password) {
		UsernamePasswordAuthenticationToken authToken = new UsernamePasswordAuthenticationToken(email, password);
		authToken.setDetails(new WebAuthenticationDetails(req));
		Authentication authentication = authenticationManager.authenticate(authToken);
		SecurityContextHolder.getContext().setAuthentication(authentication);
	}
	
	public void authWithHttpServletRequest(HttpServletRequest req, String email, String password) {
	    try {
	        req.login(email, password);
	    } catch (ServletException e) {
	    	System.out.println(e);
	    }
	}
}
