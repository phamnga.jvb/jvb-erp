package com.example.demo.resource;

import java.security.Principal;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.validation.ObjectError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.example.demo.model.AjaxJsonResponse;
import com.example.demo.model.CodeMst;
import com.example.demo.model.User;
import com.example.demo.service.CodeMstService;
import com.example.demo.service.UserService;

@Controller
public class CodeMstResource {

	@Autowired
	private CodeMstService codeMstService;
	
	@Autowired
	private MessageSource messageSource;
	
	@Autowired
	private UserService userService;
	
	@RequestMapping(value = {"/welcome"})
	public String welcome (Principal principal, HttpSession session) {
		
		if (principal != null) {
			org.springframework.security.core.userdetails.User user = (org.springframework.security.core.userdetails.User ) 
					((Authentication) principal).getPrincipal();
			String email = user.getUsername();
			List<User> listUser = userService.getUserByEmail(email);
			String userInfo = listUser.get(0).getUserName();
			session.setAttribute("userInfo", userInfo);
			session.setAttribute("user", listUser.get(0));
			
			System.out.println(session.getAttribute("user").toString());
		}
		
		return "welcome";
	}
	
	@RequestMapping(value = {"/codeMst/view"})
	public String codeMstView (Model model) {
		model.addAttribute("codeMst", new CodeMst());
		
		return "view/codeMst";
	}
	
	@RequestMapping(value = {"/codeMst/getAll"})
	@ResponseBody
	public List<CodeMst> getAll () {
		
		List<CodeMst> list = codeMstService.getAll();
		
		return list;
	}
	
	@RequestMapping(value = {"/codeMst/add"}, method = RequestMethod.POST)
	public String add (@ModelAttribute("codeMst") @Validated CodeMst codeMst,
			BindingResult bindingResult, HttpSession session, Principal principal) {
	
		// VALIDATE RESULT
		if (bindingResult.hasErrors()) {
			
			System.out.println(bindingResult.getFieldError().getDefaultMessage());
			
			return "view/codeMst";
		}
		
		List<CodeMst> listCodeMst = codeMstService.findCodeMstByCodeName(codeMst.getCodeName());
		
		if (listCodeMst.size() > 0) {
			bindingResult.addError(new ObjectError("codeName", "Duplicate.codeMst.codeName"));
			bindingResult.rejectValue("codeName", "Duplicate.codeMst.codeName");
			return "view/codeMst";
		}
		
		// GET INSERT USER + INSERT TIMESTAMP
//		System.out.println(session.getAttribute("user").toString());
//		System.out.println(session.getAttribute("user").getClass());
//		User user = (User) session.getAttribute("user");
		
		String email = "";
		
		if (principal != null) {
			org.springframework.security.core.userdetails.User user = (org.springframework.security.core.userdetails.User ) 
					((Authentication) principal).getPrincipal();
			email = user.getUsername();
			
		}
		
		Date date = new Date();
		Timestamp insertTimestamp = new Timestamp(date.getTime());
		
		codeMst.setInsertUser(email);
		codeMst.setInsertTimestamp(insertTimestamp);
		
		// INSERT TO DATABASE
		codeMstService.addCodeMst(codeMst);

		return "redirect:/codeMst/view";
	}
	
	@RequestMapping(value= {"/codeMst/findCodeMst"}, method=RequestMethod.POST)
	@ResponseBody
	public List<CodeMst> findCodeMst (HttpServletRequest req) {
		String codeType = req.getParameter("codeType"); 
		String codeName = req.getParameter("codeName");
		String codeValue = req.getParameter("codeValue");
		int sortIndex = 0;
	
		if (req.getParameter("sortIndex") != "" && req.getParameter("sortIndex") != null) {
			sortIndex = Integer.parseInt(req.getParameter("sortIndex"));
		}
		String remarks = req.getParameter("remarks"); 
		
		if (!codeType.equals("") && !codeName.equals("") && !codeValue.equals("") && sortIndex != 0 && !remarks.equals("")) {
			CodeMst codeMst = new CodeMst(codeType, codeName, codeValue, sortIndex, remarks);
			
			return codeMstService.findCodeMst(codeMst);
		}
		
		List<CodeMst> listCodeMst = new ArrayList<>();			// LIST ALL
		
		List<CodeMst> listCodeType = new ArrayList<>();
		List<CodeMst> listCodeName = new ArrayList<>();
		List<CodeMst> listCodeValue = new ArrayList<>();
		List<CodeMst> listSortIndex = new ArrayList<>();
		List<CodeMst> listRemarks = new ArrayList<>();
		
		// Add all list to listAll
		List<List<CodeMst>> listAll = new ArrayList<List<CodeMst>>();
		List<CodeMst> listTemp = new ArrayList<>();		
		
		if (!codeType.equals("")) {
			listCodeType = codeMstService.findCodeMstByCodeType(codeType);
			listAll.add(listCodeType);
		}
		
		if (!codeName.equals("")) {
			listCodeName = codeMstService.findCodeMstByCodeName(codeName);
			listAll.add(listCodeName);
		}
		
		if (!codeValue.equals("")) {
			listCodeValue = codeMstService.findCodeMstByCodeValue(codeValue);
			listAll.add(listCodeValue);
		}
		
		if (sortIndex != 0) {
			listSortIndex = codeMstService.findCodeMstBySortIndex(sortIndex);
			listAll.add(listSortIndex);
		}
			
		if (!remarks.equals("")) { 
			listRemarks = codeMstService.findCodeMstByRemarks(remarks);
			listAll.add(listRemarks);
		}

		if (listAll.size() > 0) {
			listTemp = listAll.get(0);
		}
		
		int length = listAll.size();
		
		for (CodeMst c:listTemp) {
			int index = 0;
			
			for (int i = 0; i < length; i++) {
				
				if (contains(listAll.get(i), c)) {
					index++;
				} else {
					break;
				}
			}
			
			if (index == length)
				listCodeMst.add(c);
		}

		return listCodeMst;
	}
	
	public boolean contains (List<CodeMst> list, CodeMst c) {
		
		for (CodeMst cm: list) {
			
			if (cm.getiD() == c.getiD())
				return true;
		}
		
		return false;
	}
	
	@RequestMapping(value= {"/codeMst/edit"}, method=RequestMethod.POST)
	public ResponseEntity<?> edit (HttpServletRequest req, HttpServletResponse resp,
			@Validated @RequestBody CodeMst codeMst, Errors errors,
			HttpSession session, Principal principal) {
		AjaxJsonResponse result = new AjaxJsonResponse();
		
		if (errors.hasErrors()) {
			
			List<ObjectError> listErros = errors.getAllErrors();
			
			String mess = "";
			
			for (ObjectError oE: listErros) {
				mess += messageSource.getMessage(oE, null) + ", ";
//				System.out.println(messageSource.getMessage("Size.Max.codeMst.codeType", null, null));
				System.out.println(oE);
			}
			
			result.setMsg(mess);
			System.out.println("Mess: " + mess);
			
			return ResponseEntity.badRequest().body(result);
		}
		
		List<CodeMst> list = new ArrayList<>();
		
		// UPDATE USER + TIME UPDATE
//		User user = (User) session.getAttribute("user");
		String email = "";
				
		if (principal != null) {
			org.springframework.security.core.userdetails.User user = (org.springframework.security.core.userdetails.User ) 
					((Authentication) principal).getPrincipal();
			email = user.getUsername();
		}
		
		Date date = new Date();
		Timestamp updateTimestamp = new Timestamp(date.getTime());
		
		codeMst.setUpdateUser(email);
		codeMst.setUpdateTimestamp(updateTimestamp);
		
		// EDIT OR UPDATE
		codeMstService.editCodeMst(codeMst);

		list.add(codeMst);
		result.setResult(list);
		result.setMsg("Edit successfully!");

		return ResponseEntity.ok(result);
	}
	
	@RequestMapping(value= {"/codeMst/delete"}, method=RequestMethod.POST)
	@ResponseBody
	public String delete (HttpServletRequest req) {
		String res = "Success!";
	
		String codeName = req.getParameter("codeName");
		
		codeMstService.deleteCodeMst(codeName);
		
		return res;
	}
	
	@RequestMapping(value = {"/codeMst/updatePage"}, method=RequestMethod.POST)
	@ResponseBody
	public List<CodeMst> updateBySortIndex (HttpServletRequest req, HttpSession session,
			Principal principal) throws ParseException {
		
		// GET USER 
		String email = "";
		
		if (principal != null) {
			org.springframework.security.core.userdetails.User user = (org.springframework.security.core.userdetails.User ) 
					((Authentication) principal).getPrincipal();
			email = user.getUsername();
		}
		
		// GET DATA
		SimpleDateFormat df = new SimpleDateFormat("yyyy-mm-dd'T'hh:mm:ss.sssz");
		
		List<CodeMst> listCodeMst = new ArrayList<>();
		List<CodeMst> listCodeMst2 = new ArrayList<>();
		
		// GET PARAMETER FROM REQUEST
		String currentPage_Str = req.getParameter("currentPage");
		String lengthOfEachPage_Str = req.getParameter("lengthOfEachPage");
		String start_Str = req.getParameter("start");
		String end_Str = req.getParameter("end");
		int lengthOfEachPage = Integer.parseInt(lengthOfEachPage_Str);
		int currentPage = Integer.parseInt(currentPage_Str);
		int start = Integer.parseInt(start_Str);
		int end = Integer.parseInt(end_Str);
		int lengthOfCurrentPage = end - start;
		
		List<Integer> listNewPosition = new ArrayList<>();
		
		String pos_Str = req.getParameter("listNewPosition");
		
		String[] listNewPosition1 = pos_Str.split(",");
		
		for (int j = 0; j < listNewPosition1.length; j++) {
			if (!listNewPosition1[j].equals("")) {
				listNewPosition.add(Integer.parseInt(listNewPosition1[j]));
			}
		}
		
		// IF LIST NEW POSITION NO CHANGE OR NULL ==> RETURN 
		if (!isNew(listNewPosition) || listNewPosition.size() <= 0) 			
			return listCodeMst;
		
		// ADD EACH RECORD TO LIST AND UPDATE THEM
		for (int i = 0; i < lengthOfCurrentPage; i++) {
			
			String iD_Str = req.getParameter("list[" + i + "][iD]");
			String insertTimestamp_Str  = req.getParameter("list[" + i + "][insertTimestamp]");
			String insertUser_Str  = req.getParameter("list[" + i + "][insertUser]");
			String status_Str  = req.getParameter("list[" + i + "][status]");
			String version_Str  = req.getParameter("list[" + i + "][version]");
			String codeType_Str  = req.getParameter("list[" + i + "][codeType]");
			String codeName_Str  = req.getParameter("list[" + i + "][codeName]");
			String codeValue_Str  = req.getParameter("list[" + i + "][codeValue]");
			String remarks_Str  = req.getParameter("list[" + i + "][remarks]");
			
			int iD = Integer.parseInt(iD_Str);
			Date date = new Date();
			Timestamp updateTimestamp = new Timestamp(date.getTime());
			
			Timestamp insertTimestamp = null;
			
			if (!insertTimestamp_Str.equals(""))
				insertTimestamp = new Timestamp(df.parse(insertTimestamp_Str).getTime());
			
			String updateUser = email;
			int version  = Integer.parseInt(version_Str);
			int sortIndex = lengthOfEachPage * currentPage + listNewPosition.indexOf(i);
					
			CodeMst codeMst = new CodeMst(iD, updateTimestamp, insertTimestamp, updateUser, insertUser_Str, status_Str,
					version, codeType_Str, codeName_Str, codeValue_Str, sortIndex, remarks_Str);
			
			// EDIT
			codeMstService.editCodeMstBySortIndex(codeMst);
			
			// ADD
			listCodeMst.add(codeMst);
		}
		
		for (int i = 0; i < lengthOfCurrentPage; i++) {

			listCodeMst2.add(listCodeMst.get(listNewPosition.get(i)));
		}

		return listCodeMst2;
	}
	
	public int indexOf (int[] list, int value) {
		int index = -1;
		
		for (int i = 0; i < list.length; i++) {
			if (list[i] == value)
				return i;
		}
		
		return index;
	}
	
	/**
	 * Function check list position is new or not with order: asc
	 * @param listNewPosition
	 * @return
	 */
	public boolean isNew (List<Integer> listNewPosition) {
		
		for (int i = 0; i < listNewPosition.size(); i++) {
			if (listNewPosition.get(i) != i)
				return true;
		}
		
		return false;
	}
	
}


