package com.example.demo.resource;

import java.math.BigInteger;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.example.demo.model.Terminal;
import com.example.demo.model.User;
import com.example.demo.service.TerminalService;

@Controller
public class TerminalResource {

	@Autowired
	private TerminalService terminalService;
	
	@Autowired
	private MessageSource messageSource;
	
	@RequestMapping(value = {"/terminal/view"}, method = RequestMethod.GET)
	public String view (HttpSession session) {
		
		session.removeAttribute("terminalObject");
		
		return "/view/terminal";
	}
	
	@RequestMapping(value = {"/terminal/getAll"}, method = RequestMethod.GET)
	@ResponseBody
	public List<Terminal> getAll () {
		
		return terminalService.getAll();
	}
	
	@RequestMapping(value = {"/terminal/search"}, method = RequestMethod.GET)
	@ResponseBody
	public List<Terminal> search (HttpServletRequest req) {
		String clientCode = req.getParameter("clientCode");
		String shopId = req.getParameter("shopId");
		String mtid = req.getParameter("mtid");
		String creditTid = req.getParameter("creditTid");
		String status1 = req.getParameter("status");
		String terminalModel = req.getParameter("terminalModel");
		String posModel = req.getParameter("posModel");
		String services = req.getParameter("service");
		char status = ' ';
		
		if (clientCode.equals(""))
			clientCode = null;
		
		if (shopId.equals(""))
			shopId = null;
		
		if (mtid.equals(""))
			mtid = null;
		
		if (creditTid.equals(""))
			creditTid = null;
		
		if (!status1.equals("")) 
			status = status1.charAt(0);
		
		if (terminalModel.equals(""))
			terminalModel = null;
		
		if (posModel.equals(""))
			posModel = null;
		
		if (services.equals(""))
			services = null;
		
		Terminal terminal = new Terminal(clientCode, shopId, status, mtid, creditTid, terminalModel, posModel, services);
		
		List<Terminal> list = terminalService.searchByTerminal(terminal);
		
		return list;
	}
	
	@RequestMapping(value = {"/terminal/delete"}, method = RequestMethod.POST)
	@ResponseBody
	public String deleteById (HttpServletRequest req) {
		String id1 = req.getParameter("id");
		BigInteger id = new BigInteger(id1);
		
		terminalService.deleteById(id);
		
		return "Delete Success!";
	}
	
	@RequestMapping(value = {"/terminal/verify"}, method = RequestMethod.POST)
	public ResponseEntity<?> check (@Validated @RequestBody Terminal terminal, Errors errors,
			HttpServletResponse resp, HttpSession session) {
		
		String mtid = terminal.getMtid();
		List<Terminal> listTerminalByMtid = terminalService.getTerminalByMtid(mtid);
		
		if (listTerminalByMtid.size() > 1
				|| (listTerminalByMtid.size() == 1 &&  terminal.getId() != null && (listTerminalByMtid.get(0).getId().compareTo(terminal.getId()) != 0))
				|| (listTerminalByMtid.size() == 1 &&  terminal.getId() == null)) {

			System.out.println("Id Terminal: " + terminal.getId());
			System.out.println("Id Terminal 2: " + listTerminalByMtid.get(0).getId());
			
			errors.rejectValue("mtid", "Duplicate.terminal.mtid");
		}
		
		//---------------------------------//
		//			IF ERRORS			   //
		//---------------------------------//
		if (errors.hasErrors()) {
			List<ObjectError> listErrors = errors.getAllErrors();
			String mess = "";
			
			for (ObjectError error: listErrors) {
				FieldError fieldError = (FieldError) error;
				mess += fieldError.getField() + ": " + messageSource.getMessage(fieldError, null) + ", ";
			}
			
			System.out.println("Mess: " + mess);
			
			return ResponseEntity.badRequest().body(mess);
		}
		
		//-----------------------------------------//
		//				ELSE NOT ERRORS	 		   //
		//-----------------------------------------//
		LocalDate reqDeliveryDate = null;
		LocalDate cancelDate = null;
		LocalDate reqInstallDate = null;
		LocalDate reqStartDate = null;
		LocalDate startDate = null;
				
		if (terminal.getReqDeliveryDate() != null) {
			reqDeliveryDate = terminal.getReqDeliveryDate().toLocalDate();
		} 
		
		if (terminal.getCancelDate() != null) {
			cancelDate = terminal.getCancelDate().toLocalDate();
		}
		
		if (terminal.getReqInstallDate() != null) {
			reqInstallDate = terminal.getReqInstallDate().toLocalDate();
		}
		
		if (terminal.getReqStartDate() != null) {
			reqStartDate = terminal.getReqStartDate().toLocalDate();
		}
		
		if (terminal.getStartDate() != null) {
			startDate = terminal.getStartDate().toLocalDate();
		}
		
		session.setAttribute("terminalObject", terminal);
		session.setAttribute("reqDeliveryDate", reqDeliveryDate);
		session.setAttribute("cancelDate", cancelDate);
		session.setAttribute("reqInstallDate", reqInstallDate);
		session.setAttribute("reqStartDate", reqStartDate);
		session.setAttribute("startDate", startDate);
				
		System.out.println("Mess: " + "Ok!");
		
		return ResponseEntity.ok("{\"message\": \"Check success!\"}");
	}

	@RequestMapping(value = {"/terminal/add"}, method = RequestMethod.POST)
	public ResponseEntity<?> add (@RequestBody Terminal terminal, HttpSession session) {
		User user = (User) session.getAttribute("user");
		String email = user.getEmail();
		java.util.Date date = new java.util.Date();
		Timestamp insertTimestamp = new Timestamp(date.getTime());
		
		terminal.setInsertUser(email);
		terminal.setInsertTimestamp(insertTimestamp);
		
		// ADD TO DATABASE
		terminalService.add(terminal);
		
		session.removeAttribute("terminalObject");
		session.removeAttribute("reqDeliveryDate");
		session.removeAttribute("cancelDate");
		session.removeAttribute("reqInstallDate");
		session.removeAttribute("reqStartDate");
		session.removeAttribute("startDate");
		
		return ResponseEntity.ok("{\"message\": \"Add success!\"}");
	}
	
	@RequestMapping(value = {"/terminal/view-add"}, method = RequestMethod.GET)
	public String viewAdd (Model model, HttpSession session) {
		
		if (session.getAttribute("terminalObject") != null) {
			Terminal terminal = (Terminal) session.getAttribute("terminalObject");
			
			LocalDate reqDeliveryDate = null;
			LocalDate cancelDate = null;
			LocalDate reqInstallDate = null;
			LocalDate reqStartDate = null;
			LocalDate startDate = null;
					
			if (terminal.getReqDeliveryDate() != null) {
				reqDeliveryDate = terminal.getReqDeliveryDate().toLocalDate();
			} 
			
			if (terminal.getCancelDate() != null) {
				cancelDate = terminal.getCancelDate().toLocalDate();
			}
			
			if (terminal.getReqInstallDate() != null) {
				reqInstallDate = terminal.getReqInstallDate().toLocalDate();
			}
			
			if (terminal.getReqStartDate() != null) {
				reqStartDate = terminal.getReqStartDate().toLocalDate();
			}
			
			if (terminal.getStartDate() != null) {
				startDate = terminal.getStartDate().toLocalDate();
			}
		
			model.addAttribute("terminalObject", terminal);
			model.addAttribute("reqDeliveryDate", reqDeliveryDate);
			model.addAttribute("cancelDate", cancelDate);
			model.addAttribute("reqInstallDate", reqInstallDate);
			model.addAttribute("reqStartDate", reqStartDate);
			model.addAttribute("startDate", startDate);
		}
		
		return "add/terminal-add";
	}
	
	@RequestMapping(value = {"/terminal/view-add-verify"}, method = RequestMethod.GET)
	public String viewAddVerify (Model model) {
		
		return "/add/terminal-add-verify";
	}
		
	/**
	 * View first Update Window
	 * @param terminal
	 * @param model
	 * @return
	 */
	@RequestMapping(value = {"/terminal/view-update"}, method = RequestMethod.POST)
	@ResponseBody
	public String viewUpdate (@RequestBody Terminal terminal, Model model, HttpSession session) {
			
		LocalDate reqDeliveryDate = null;
		LocalDate cancelDate = null;
		LocalDate reqInstallDate = null;
		LocalDate reqStartDate = null;
		LocalDate startDate = null;

		if (terminal.getReqDeliveryDate() != null) {
			reqDeliveryDate = terminal.getReqDeliveryDate().toLocalDate();
		}

		if (terminal.getCancelDate() != null) {
			cancelDate = terminal.getCancelDate().toLocalDate();
		}

		if (terminal.getReqInstallDate() != null) {
			reqInstallDate = terminal.getReqInstallDate().toLocalDate();
		}

		if (terminal.getReqStartDate() != null) {
			reqStartDate = terminal.getReqStartDate().toLocalDate();
		}

		if (terminal.getStartDate() != null) {
			startDate = terminal.getStartDate().toLocalDate();
		}

		model.addAttribute("reqDeliveryDate", reqDeliveryDate);
		model.addAttribute("cancelDate", cancelDate);
		model.addAttribute("reqInstallDate", reqInstallDate);
		model.addAttribute("reqStartDate", reqStartDate);
		model.addAttribute("startDate", startDate);

		session.setAttribute("terminalObject", terminal);
		
		session.setAttribute("reqDeliveryDate", reqDeliveryDate);
		session.setAttribute("cancelDate", cancelDate);
		session.setAttribute("reqInstallDate", reqInstallDate);
		session.setAttribute("reqStartDate", reqStartDate);
		session.setAttribute("startDate", startDate);

		session.setAttribute("terminalObject", terminal);
		
		return "Open first window update!";
	}
	
	/**
	 * View first Update Window
	 * @param terminal
	 * @param model
	 * @return
	 */
	@RequestMapping(value = {"/terminal/view-update-2"}, method = RequestMethod.GET)
	public String viewUpdate (Model model, HttpSession session) {
		Terminal terminal = (Terminal) session.getAttribute("terminalObject");
		
		LocalDate reqDeliveryDate = null;
		LocalDate cancelDate = null;
		LocalDate reqInstallDate = null;
		LocalDate reqStartDate = null;
		LocalDate startDate = null;

		if (terminal.getReqDeliveryDate() != null) {
			reqDeliveryDate = terminal.getReqDeliveryDate().toLocalDate();
		}

		if (terminal.getCancelDate() != null) {
			cancelDate = terminal.getCancelDate().toLocalDate();
		}

		if (terminal.getReqInstallDate() != null) {
			reqInstallDate = terminal.getReqInstallDate().toLocalDate();
		}

		if (terminal.getReqStartDate() != null) {
			reqStartDate = terminal.getReqStartDate().toLocalDate();
		}

		if (terminal.getStartDate() != null) {
			startDate = terminal.getStartDate().toLocalDate();
		}

		model.addAttribute("reqDeliveryDate", reqDeliveryDate);
		model.addAttribute("cancelDate", cancelDate);
		model.addAttribute("reqInstallDate", reqInstallDate);
		model.addAttribute("reqStartDate", reqStartDate);
		model.addAttribute("startDate", startDate);
		model.addAttribute("terminalObject", terminal);

		return "update/terminal-update";
	}
	
	/**
	 * View second Update Window
	 * @param terminal
	 * @param model
	 * @return
	 */
	@RequestMapping(value = {"/terminal/view-update-verify"}, method = RequestMethod.GET)
	public String viewUpdateVerify () {
		
		return "/update/terminal-update-verify";
	}
	
	/**
	 * Main Update
	 * @param terminal
	 * @return
	 */
	@RequestMapping(value = {"/terminal/update"}, method=RequestMethod.POST)
	public ResponseEntity<?> terminalUpdate (@RequestBody Terminal terminal, HttpSession session) {
		
		User user = (User) session.getAttribute("user");
		String email = user.getEmail();
		java.util.Date date = new java.util.Date();
		Timestamp updateTimestamp = new Timestamp(date.getTime());
		
		terminal.setUpdateUser(email);
		terminal.setUpdateTimestamp(updateTimestamp);
		
		// UPDATE TERMINAL IN DATABASE
		terminalService.updateById(terminal);
		
		session.removeAttribute("terminalObject");
		
		return ResponseEntity.ok("{\"message\": \"Update success!\"}");
	}
	
}
